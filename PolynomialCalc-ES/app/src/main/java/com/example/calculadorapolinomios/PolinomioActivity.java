package com.example.calculadorapolinomios;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PolinomioActivity extends Activity {

    private TextView txtResultado, txtProvisional;
    private Button botonUno, botonDos, botonTres, botonCuatro, botonCinco, botonSeis, botonSiete, botonOcho, botonNueve, botonSumar, botonRestar, botonBorrar, botonEnter, botonMultiplicar;

    private Operacion tipoOP;
    private Polinomio resultado = new Polinomio();
    private Polinomio provisional = new Polinomio();

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.polinomios);

        mapUI();
    }

    private void mapUI() {
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        txtProvisional = (TextView) findViewById(R.id.txtProvisional);

        botonUno = (Button) findViewById(R.id.botonUno);
        botonDos = (Button) findViewById(R.id.botonDos);
        botonTres = (Button) findViewById(R.id.botonTres);
        botonCuatro = (Button) findViewById(R.id.botonCuatro);
        botonCinco = (Button) findViewById(R.id.botonCinco);
        botonSeis = (Button) findViewById(R.id.botonSeis);
        botonSiete = (Button) findViewById(R.id.botonSiete);
        botonOcho = (Button) findViewById(R.id.botonOcho);
        botonNueve = (Button) findViewById(R.id.botonNueve);

        botonBorrar = (Button) findViewById(R.id.botonBorrar);
        botonSumar = (Button) findViewById(R.id.botonSumar);
        botonRestar = (Button) findViewById(R.id.botonRestar);
        botonMultiplicar = (Button) findViewById(R.id.botonMultiplicar);
        botonEnter = (Button) findViewById(R.id.botonEnter);
    }

    public void setNumber(View view) {
        int id = view.getId();

        if (id == botonUno.getId()) {
            provisional.add(1);
        } else if (id == botonDos.getId()) {
            provisional.add(2);
        } else if (id == botonTres.getId()) {
            provisional.add(3);
        } else if (id == botonCuatro.getId()) {
            provisional.add(4);
        } else if (id == botonCinco.getId()) {
            provisional.add(5);
        } else if (id == botonSeis.getId()) {
            provisional.add(6);
        } else if (id == botonSiete.getId()) {
            provisional.add(7);
        } else if (id == botonOcho.getId()) {
            provisional.add(8);
        } else if (id == botonNueve.getId()) {
            provisional.add(9);
        }

        else if (id == botonBorrar.getId()) {
            provisional.borrar();
        }

        txtProvisional.setText(provisional.getResultado());
    }

    public void setFunctions(View view) {
        int id = view.getId();

        if (id == botonSumar.getId()) {
            tipoOP = Operacion.SUMA;
        } else if (id == botonRestar.getId()) {
            tipoOP = Operacion.RESTA;
        } else if (id == botonMultiplicar.getId()) {
            tipoOP = Operacion.MULTIPLICACION;
        }

        if (tipoOP != null) {
            resultado.operacion(provisional, tipoOP);
        }

        txtResultado.setText(resultado.getResultado());
        provisional.borrar();
        txtProvisional.setText("");
    }
}