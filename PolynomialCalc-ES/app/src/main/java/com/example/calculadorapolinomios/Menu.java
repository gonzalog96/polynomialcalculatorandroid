package com.example.calculadorapolinomios;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Menu extends Activity implements View.OnClickListener{
    private Button botonPolinomio;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.main);

        botonPolinomio = (Button) findViewById(R.id.botonPolinomio);
        botonPolinomio.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == botonPolinomio.getId()) {
            Intent i = new Intent(this, PolinomioActivity.class);
            startActivity(i);
        }
    }
}
