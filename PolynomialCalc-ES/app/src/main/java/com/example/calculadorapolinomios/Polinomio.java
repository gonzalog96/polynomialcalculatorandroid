package com.example.calculadorapolinomios;

import java.util.Collections;
import java.util.Vector;

public class Polinomio {
    private Vector<Integer> polinomio = new Vector();

    public void add(int elemento) {
        polinomio.add(elemento);
    }

    public String getResultado() {
        StringBuilder resultado = new StringBuilder();

        for (int i=0; i < polinomio.size(); i++) {
            resultado.append(polinomio.get(i));

            if (i != 0) {
                resultado.append("x^" + i);
            }

            if (i != polinomio.size() - 1) {
                resultado.append("+");
            }
        }

        return resultado.toString();
    }

    private int tamanio(Polinomio segundo) {
        return polinomio.size() > segundo.polinomio.size() ? polinomio.size() : segundo.polinomio.size();
    }

    public void operacion(Polinomio sumando, Operacion operacion) {
        Vector<Integer> sumandoVector = sumando.polinomio;

        int tam = tamanio(sumando);

        for (int i=0; i < tam; i++) {
            if (i < polinomio.size() && i < sumandoVector.size()) {
                int resultado = 0;
                if (operacion == Operacion.SUMA) {
                    resultado = polinomio.get(i) + sumandoVector.get(i);
                } else if (operacion == Operacion.MULTIPLICACION) {
                    resultado = polinomio.get(i) * sumandoVector.get(i);
                } else if (operacion == Operacion.RESTA) {
                    resultado = polinomio.get(i) - sumandoVector.get(i);
                }
                polinomio.set(i, resultado);
            } else if (i < sumandoVector.size()) {
                polinomio.add(sumandoVector.get(i));
            }
        }
    }

    public void borrar() {
        polinomio.removeAllElements();
    }
}
